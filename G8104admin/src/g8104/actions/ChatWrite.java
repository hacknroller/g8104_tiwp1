package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.business.admin.AdminBusinessDefault;
import g8104.business.contact.*;


public class ChatWrite implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		if(!new AdminBusinessDefault().adminIsLoggedin(request)) return new HomePage().handleRequest(request, response);
		
		new ContactBusinessDefault().escrituraJMS(request);
		return "contact.jsp";
	}
}
