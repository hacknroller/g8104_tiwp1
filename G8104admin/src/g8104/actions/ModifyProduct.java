package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.business.admin.AdminBusinessDefault;
import g8104.business.product.ProductBusinessDefault;
import g8104.data.Producto;

public class ModifyProduct implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se comprueba si esta logueado
		if(!new AdminBusinessDefault().adminIsLoggedin(request)) return new HomePage().handleRequest(request, response);
				
		Producto product = new ProductBusinessDefault().modifyProduct(request);
		
		if(product != null){
			request.setAttribute("product", product);
			return "productDetails.jsp";
		}
		else
			return new AdminProductsPage().handleRequest(request, response);
	}
}