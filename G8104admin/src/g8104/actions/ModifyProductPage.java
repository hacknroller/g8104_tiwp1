package g8104.actions;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.business.admin.AdminBusinessDefault;
import g8104.business.product.ProductBusinessDefault;
import g8104.data.Producto;

public class ModifyProductPage implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se comprueba si esta logueado
		if(!new AdminBusinessDefault().adminIsLoggedin(request)) return new HomePage().handleRequest(request, response);
				
		//Se obtiene el producto a mostrar
		List<Producto> product = new ProductBusinessDefault().findProductById(request);
		request.setAttribute("product", product.get(0));
		
		return "productDetails.jsp";
	}
}