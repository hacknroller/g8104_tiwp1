package g8104.actions;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.business.admin.AdminBusinessDefault;
import g8104.business.user.UserBusinessDefault;
import g8104.data.Usuario;

public class ModifyUserPage implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se comprueba si esta logueado
		if(!new AdminBusinessDefault().adminIsLoggedin(request)) return new HomePage().handleRequest(request, response);
						
		//Se obtiene el usuario deseado para modificar
		Usuario user = new UserBusinessDefault().findUserByEmail(request);
		
		//Se redirecciona correctamente
		if(user != null){
			request.setAttribute("user", user);
			return "userDetails.jsp";
		}
		else return new AdminUsersPage().handleRequest(request, response);
	}
}