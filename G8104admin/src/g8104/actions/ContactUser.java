package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import g8104.business.admin.AdminBusinessDefault;
import g8104.business.user.UserBusinessDefault;
import g8104.data.DataProxy;
import g8104.data.Usuario;



public class ContactUser implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		if(!new AdminBusinessDefault().adminIsLoggedin(request)) return new HomePage().handleRequest(request, response);
		
		String email = (String) request.getParameter("userLector");
		
		if (new UserBusinessDefault().findUserByStringEmail(email) ==null ){
			return new AdminAccountPage().handleRequest(request, response);
		}else {
			request.getSession(false).setAttribute("userLector",email);
			System.out.println(request.getParameter("userLector"));
			return "contact.jsp";
		}
	}
}
