package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.business.admin.AdminBusinessDefault;

public class HomePage implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se comprueba si esta logueado
		if(new AdminBusinessDefault().adminIsLoggedin(request)) return new AdminAccountPage().handleRequest(request, response);
		return "index.jsp";
	}
}