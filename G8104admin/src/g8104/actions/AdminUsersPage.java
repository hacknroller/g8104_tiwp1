package g8104.actions;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.business.admin.AdminBusinessDefault;
import g8104.business.user.UserBusinessDefault;
import g8104.data.Usuario;

public class AdminUsersPage implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se comprueba si esta logueado
		if(!new AdminBusinessDefault().adminIsLoggedin(request)) return new HomePage().handleRequest(request, response);
				
		List<Usuario> listOfUsers = new UserBusinessDefault().findAllUsers();
		
		if(!listOfUsers.isEmpty()){
			request.setAttribute("listOfUsers", listOfUsers);
			request.setAttribute("lastUser", listOfUsers.size()-1);
		}
		else request.setAttribute("noUsersYet", "No hay usuarios registrados por el momento");
		
		return "adminUsers.jsp";
	}
}