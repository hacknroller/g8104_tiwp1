package g8104.actions;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.business.admin.AdminBusinessDefault;
import g8104.business.product.ProductBusinessDefault;
import g8104.data.Producto;


public class AdminProductsPage implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se comprueba si esta logueado
		if(!new AdminBusinessDefault().adminIsLoggedin(request)) return new HomePage().handleRequest(request, response);
		
		List<Producto> listOfProducts = new ProductBusinessDefault().findAllProducts();
		
		if(!listOfProducts.isEmpty()){
			request.setAttribute("listOfProducts", listOfProducts);
			request.setAttribute("lastProduct", listOfProducts.size()-1);
		}
		else request.setAttribute("noProductsYet", "No articulos registrados por el momento");
		
		return "adminProducts.jsp";
	}
}