package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.business.admin.AdminBusinessDefault;
import g8104.business.user.UserBusinessDefault;

public class RemoveUser implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se comprueba si esta logueado
		if(!new AdminBusinessDefault().adminIsLoggedin(request)) return new HomePage().handleRequest(request, response);
				
		if(new UserBusinessDefault().removeUser(request)) return new AdminUsersPage().handleRequest(request, response);
		else request.setAttribute("removeFailMessage", "La password introducida es incorrecta");
		
		return "userDetails.jsp";
	}
}