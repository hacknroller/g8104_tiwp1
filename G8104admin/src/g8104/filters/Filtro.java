package g8104.filters;

import java.io.IOException;
import java.util.HashMap;


import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.actions.AdminProductsPage;
import g8104.actions.AdminUsersPage;
import g8104.actions.HomePage;
import g8104.actions.Login;
import g8104.actions.Logout;
import g8104.actions.ModifyProduct;
import g8104.actions.ModifyProductPage;
import g8104.actions.ModifyUser;
import g8104.actions.ModifyUserPage;
import g8104.actions.RemoveProduct;
import g8104.actions.RemoveUser;




/**
 * Servlet Filter implementation class Filtro
 */
@WebFilter("/*")
public class Filtro implements Filter {
    public Filtro() {

    }
	public void destroy() {

	}

	private HashMap<String, Boolean> handlerHash = new HashMap<String, Boolean>();
	
	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = ((HttpServletRequest)request);
		String url = req.getServletPath();
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		
		if(url.contains(".jsp")){
			request.getRequestDispatcher("error.jsp").forward(request, response);
		}
				
		chain.doFilter(request, httpResponse);
		return;
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		handlerHash.put("/homePage", true);
		handlerHash.put("/logout", true);
		handlerHash.put("/login", true);
		handlerHash.put("/adminUsersPage",true);
		handlerHash.put("/adminProductsPage",true);
		handlerHash.put("/modifyUserPage",true);
		handlerHash.put("/modifyUser",true);
		handlerHash.put("/removeUser", true);
		handlerHash.put("/modifyProductPage", true);
		handlerHash.put("/modifyProduct",true);
		handlerHash.put("/removeProduct",true);
	}
}