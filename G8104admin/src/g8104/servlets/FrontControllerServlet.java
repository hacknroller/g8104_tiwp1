package g8104.servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import g8104.actions.Action;
import g8104.actions.ChatRead;
import g8104.actions.ChatWrite;
import g8104.actions.ContactUser;
import g8104.actions.AdminAccountPage;
import g8104.actions.AdminProductsPage;
import g8104.actions.AdminUsersPage;
import g8104.actions.HomePage;
import g8104.actions.Login;
import g8104.actions.Logout;
import g8104.actions.ModifyProduct;
import g8104.actions.ModifyProductPage;
import g8104.actions.ModifyUser;
import g8104.actions.ModifyUserPage;
import g8104.actions.RemoveProduct;
import g8104.actions.RemoveUser;

/**
 * Servlet central que controla todo el trafico de la aplicacion
 */
@WebServlet(name = "FrontControllerServlet", 
			urlPatterns ={"/G8104admin",
					"/homePage",
					"/chatRead",
					"/chatWrite",
					"/contactUser",
					"/adminAccountPage",
					"/login",
					"/logout",
					"/adminUsersPage",
					"/adminProductsPage",
					"/modifyUserPage",
					"/modifyUser",
					"/removeUser",
					"/modifyProductPage",
					"/modifyProduct",
					"/removeProduct"})
@MultipartConfig
public class FrontControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public FrontControllerServlet() {
        super();
    }
	private HashMap<String, Action> GEThandlerHash = new HashMap<String, Action>();
	private HashMap<String, Action> POSThandlerHash = new HashMap<String, Action>();

	public void init(ServletConfig config) throws ServletException {
		GEThandlerHash.put("/homePage", new HomePage());
		GEThandlerHash.put("/logout", new Logout());
		GEThandlerHash.put("/adminAccountPage", new AdminAccountPage());
		POSThandlerHash.put("/login", new Login());
		POSThandlerHash.put("/contactUser", new ContactUser());
		POSThandlerHash.put("/chatWrite", new ChatWrite());
		POSThandlerHash.put("/chatRead", new ChatRead());
		POSThandlerHash.put("/adminUsersPage", new AdminUsersPage());
		POSThandlerHash.put("/adminProductsPage", new AdminProductsPage());
		POSThandlerHash.put("/modifyUserPage", new ModifyUserPage());
		POSThandlerHash.put("/modifyUser", new ModifyUser());
		POSThandlerHash.put("/removeUser", new RemoveUser());
		POSThandlerHash.put("/modifyProductPage", new ModifyProductPage());
		POSThandlerHash.put("/modifyProduct", new ModifyProduct());
		POSThandlerHash.put("/removeProduct", new RemoveProduct());
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se obtiene la clase que implementa la accion
		Action requestHandler = (Action) GEThandlerHash.get(request.getServletPath());
		
		//Si el servicio no existe
		if(requestHandler == null){
			//Se lanza error 404
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
		else{
			//Si el servicio existe se ejecuta la accion que lo gestiona
			String url = requestHandler.handleRequest(request, response);
			
			//Se redirecciona a la vista adecuada
			if(url == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND);				
			}
			else{
				request.getRequestDispatcher(url).forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se obtiene la clase que implementa la accion
		Action requestHandler = (Action) POSThandlerHash.get(request.getServletPath());
		
		//Si el servicio no existe
		if(requestHandler == null){
			//Se lanza error 404
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
		else{
			//Si el servicio existe se ejecuta la accion que lo gestiona
			String url = requestHandler.handleRequest(request, response);
			
			//Se redirecciona a la vista adecuada
			if(url == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND);				
			}
			else{
				request.getRequestDispatcher(url).forward(request, response);
			}
		}
	}
}