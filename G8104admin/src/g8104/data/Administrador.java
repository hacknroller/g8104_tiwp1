package g8104.data;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the ADMINISTRADOR database table.
 * 
 */
@Entity
@NamedQuery(name="findAdmin",query="SELECT a FROM Administrador a WHERE a.email=?1")
public class Administrador implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String email;

	private String password;

	public Administrador() {
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}