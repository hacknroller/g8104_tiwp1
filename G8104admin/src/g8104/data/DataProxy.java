package g8104.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DataProxy {

	@SuppressWarnings("unchecked")
	public List<Administrador> findAdmin(String email){
		EntityManager em = createConnection();
		
		List<Administrador> result =  em.createNamedQuery("findAdmin").setParameter(1, email).getResultList();
		
		closeConnection(em);
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> findAllUsers(){
		EntityManager em = createConnection();
		
		List<Usuario> result =  em.createNamedQuery("findAllUsers").getResultList();
		
		closeConnection(em);
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> findUserByEmail(String email){
		EntityManager em = createConnection();
		
		List<Usuario> result =  em.createNamedQuery("findUserByEmail").setParameter(1, email).getResultList();
		
		closeConnection(em);
		
		return result;
	}
	
	/**
	 * Busca a un usario en la base de datos por su clave primaria
	 * 
	 * @param email - String con email del usuario
	 * @return La instancia del usuario, y null si no existe
	 */
	@SuppressWarnings("unchecked")
	public List<Usuario> findUser(String email){
		EntityManager em = createConnection();
		
		List<Usuario> result =  em.createNamedQuery("findUser").setParameter(1, email).getResultList();
		
		closeConnection(em);
		
		return result;
	}
	
	
	public Usuario updateUser(Usuario userChanges, String email){
		EntityManager em = createConnection();
		
		//Se busca al usuario que desea cambiar sus credenciales en la base de datos
		Usuario newUser = em.find(Usuario.class,email);
				
		//Se actualizan sus valores
		newUser.setName(userChanges.getName());
		newUser.setFirstsurname(userChanges.getFirstsurname());
		newUser.setSecondsurname(userChanges.getSecondsurname());
		newUser.setCity(userChanges.getCity());
		newUser.setPassword(userChanges.getPassword());
		
		//Se comitea resultados
		commit(em);
		
		closeConnection(em);
		
		return newUser;
	}
	
	/**
	 * Elimina a un usario en la base de datos por su clave primaria
	 * 
	 * @param email - String con email del usuario
	 * @return true si ha sido eliminado y false si no.
	 */
	public boolean removeUser(String email){
		EntityManager em = createConnection();
		Usuario usuario=em.find(Usuario.class, email);
			em.remove(usuario);
			//Se comitea resultados
			commit(em);
		  closeConnection(em);
		  return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<Producto> findProductById(int id){
		EntityManager em = createConnection();
		
		List<Producto> result =  em.createNamedQuery("findProductById").setParameter(1, id).getResultList();
		
		closeConnection(em);
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Producto> findAllProducts(){
		EntityManager em = createConnection();
		
		List<Producto> result =  em.createNamedQuery("findAllProducts").getResultList();
		
		closeConnection(em);
		
		return result;
	}
	
	public Producto updateProduct(Producto productoChanges){
		EntityManager em = createConnection();
		
		//Se busca al usuario que desea cambiar sus credenciales en la base de datos
		Producto newProduct = em.find(Producto.class,productoChanges.getIdproducto());
				
		//Se actualizan sus valores
		newProduct.setTitle(productoChanges.getTitle());
		newProduct.setState(productoChanges.getState());
		newProduct.setCategory(productoChanges.getCategory());
		newProduct.setDescription(productoChanges.getDescription());
		newProduct.setPrice(productoChanges.getPrice());
		newProduct.setPicture(productoChanges.getPicture());
		
		//Se comitea resultados
		commit(em);
		
		closeConnection(em);
		
		return newProduct;
	}
	
	/**
	 * Elimina a un producto en la base de datos por su clave primaria
	 * 
	 * @param email - String con email del usuario
	 * @return true si ha sido eliminado y false si no.
	 */
	public boolean removeProduct(int idProducto){
		EntityManager em = createConnection();
		Producto producto=em.find(Producto.class, idProducto);
			em.remove(producto);
			//Se comitea resultados
			commit(em);
		  closeConnection(em);
		  return true;
	}
	
	/**
	 * Crea una conexion con la base de datos e inicia una transaccion
	 * 
	 * @return objeto EntityManager asociado
	 */
	private EntityManager createConnection(){
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("G8104wallapop");
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		
		return em;
	}
	
	/**
	 * Cierra una conexion con la base de datos
	 * @param em - objeto EntityManager asociado
	 */
	private void closeConnection(EntityManager em){
		em.close();
	}
	
	/**
	 * Comitea en la base de datos
	 * @param em - objeto EntityManager asociado
	 */
	private void commit(EntityManager em){
		em.getTransaction().commit();
	}

}
