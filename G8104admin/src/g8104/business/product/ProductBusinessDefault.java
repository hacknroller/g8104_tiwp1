package g8104.business.product;

import java.io.IOException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import g8104.data.DataProxy;
import g8104.data.Producto;

public class ProductBusinessDefault implements ProductBusiness {
	/**
	 * Proxy to communicate with data
	 */
	private DataProxy dp = new DataProxy();
	
	@Override
	public List<Producto> findAllProducts() {
		List<Producto> result = dp.findAllProducts();
		return result;
	}
	
	@Override
	public List<Producto> findProductById(HttpServletRequest request) {
		//Se obtiene el id del producto
		String id = request.getParameter("productToModify");
		if(id == null)
			return null;
		
		//Se obtiene el producto
		List<Producto> result = dp.findProductById(Integer.parseInt(id));
		return result;
	}
	
	@Override
	public Producto modifyProduct(HttpServletRequest request) {
		//Se obtiene el producto a modificar
		List<Producto> productChanges = dp.findProductById(Integer.parseInt(request.getParameter("productToModify")));

		if(!request.getParameter("titleChange").equals("")) productChanges.get(0).setTitle(request.getParameter("titleChange"));
		if(request.getParameter("estadoChange") != null) productChanges.get(0).setState(request.getParameter("estadoChange"));
		if(request.getParameter("categoriaChange") != null) productChanges.get(0).setCategory(request.getParameter("categoriaChange"));
		if(!request.getParameter("descriptionChange").equals("")) productChanges.get(0).setDescription(request.getParameter("descriptionChange"));
		if(!request.getParameter("priceChange").equals("")) productChanges.get(0).setPrice(request.getParameter("priceChange"));
		
		//Update image
		try {
			if(!request.getPart("file").getSubmittedFileName().equals("")){
				String image="defaultProduct.jpg";
				try {
					image = new g8104.business.image.ImageBusinessDefault().uploadImage(request, productChanges.get(0).getIdproducto()+"");
				} catch (IOException e) {
					e.printStackTrace();
					return null;
				} catch (ServletException e) {
					e.printStackTrace();
					return null;
				} catch (NamingException e) {
					e.printStackTrace();
					return null;
				}
				productChanges.get(0).setPicture(image);			
			}
		} catch (IOException | ServletException e) {
			e.printStackTrace();
		}

		//Se retorna el producto actualizado
		Producto newProduct = dp.updateProduct(productChanges.get(0));
		return newProduct;
	}
	
	@Override
	public boolean removeProduct(HttpServletRequest request) {
		//Se obtiene el producto a modificar
		List<Producto> products = dp.findProductById(Integer.parseInt(request.getParameter("productToModify")));
		Producto productDelete=products.get(0);
		int idDelete=productDelete.getIdproducto();
		dp.removeProduct(idDelete);
		return true;
	}
}