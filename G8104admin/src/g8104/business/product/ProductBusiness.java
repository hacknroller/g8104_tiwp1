package g8104.business.product;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import g8104.data.Producto;

public interface ProductBusiness {
	public List<Producto> findAllProducts();
	
	public List<Producto> findProductById(HttpServletRequest request);
	
	public Producto modifyProduct(HttpServletRequest request);
	
	public boolean removeProduct(HttpServletRequest request);
}