package g8104.business.user;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import g8104.data.DataProxy;
import g8104.data.Usuario;

public class UserBusinessDefault implements UserBusiness {
	/**
	 * Proxy to communicate with data
	 */
	private DataProxy dp = new DataProxy();
	
	@Override
	public List<Usuario> findAllUsers() {
		List<Usuario> result = dp.findAllUsers();
		return result;
	}

	@Override
	public Usuario findUserByEmail(HttpServletRequest request) {
		if(request.getParameter("userToModify") != null){
			List<Usuario> result = dp.findUserByEmail(request.getParameter("userToModify"));
			if(!result.isEmpty()){
				return result.get(0);
			}
			else return null;
		}
		return null;
	}
	
	@Override
	public Usuario findUserByStringEmail(String email) {
		List<Usuario> result = dp.findUserByEmail(email);
		if(!result.isEmpty()){
			return result.get(0);
		}
		else return null;
	}
	
	@Override
	public boolean modifyUser(HttpServletRequest request) {
		boolean result = true;
		//Se obtiene el atributo usuario
		String email = request.getParameter("userToModify");
		Usuario userChanges = (Usuario) dp.findUserByEmail(email).get(0);


		//Se comprueban los campos a actualizar
		if(!request.getParameter("nameChange").equals("")) userChanges.setName(request.getParameter("nameChange"));
		if(!request.getParameter("firstsurnameChange").equals("")) userChanges.setFirstsurname(request.getParameter("firstsurnameChange"));
		if(!request.getParameter("secondsurnameChange").equals("")) userChanges.setSecondsurname(request.getParameter("secondsurnameChange"));
		if(!request.getParameter("ciudad").equals("")) userChanges.setCity(request.getParameter("ciudad"));
		
		//Si se cambia la contrasena, la antigua debe coincidir
		String passCifrada = cifrarPsw(request.getParameter("oldPasswordChange"));
		if(passCifrada.equals(userChanges.getPassword()) && !request.getParameter("newPasswordChange").equals(""))
			userChanges.setPassword(cifrarPsw(request.getParameter("newPasswordChange")));
		else if (!cifrarPsw(request.getParameter("oldPasswordChange")).equals(userChanges.getPassword()) && !request.getParameter("newPasswordChange").equals(""))
			result = false;

		//Se actualizan los valores en la base de datos
		Usuario newUser = dp.updateUser(userChanges, email);
		
		//Se actualiza el atributo en la pagina
		request.setAttribute("user", newUser);
		
		return result;
	}
	
	@Override
	public boolean removeUser(HttpServletRequest request) {
		//Se obtiene el atributo usuario
		String email = request.getParameter("userToModify");
		Usuario userChanges = (Usuario) dp.findUserByEmail(email).get(0);
		//obtiene la contrase�a del usuario
		String sessionPassword = userChanges.getPassword();
		//obtiene la contrase�a introducida por el usuario
		String requestPassword=request.getParameter("passwordDelete");
		//hash de la contrase�a del request
		String hashRequestPassword=cifrarPsw(requestPassword);
		if(sessionPassword.equals(hashRequestPassword)==false){
			request.setAttribute("user", userChanges);
			return false;
		}
		//si las contrasenias coinciden se elimina la cuenta
		dp.removeUser(email);

		return true;
	}
	
	/**
	 * Devuelve la contraseña cifrada con hash md5
	 * 
	 * @param contraseña
	 * @return contraseña cifrada
	 */
   private String cifrarPsw(String contrasena) {
        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(contrasena.getBytes());
            //Get the hash's bytes 
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        } 
        catch (NoSuchAlgorithmException e) 
        {
            e.printStackTrace();
        }
        return generatedPassword;
    }
}