package g8104.business.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import g8104.data.Usuario;

public interface UserBusiness {
	/**
	 * Busca todos los usuarios registrados
	 * @return lista de los usuarios registrados
	 */
	public List<Usuario> findAllUsers();
	
	public Usuario findUserByEmail(HttpServletRequest request);
	
	public boolean modifyUser(HttpServletRequest request);
	
	public Usuario findUserByStringEmail(String email);
	
	/**
	 * Elimina al usuario
	 * 
	 * @param request - Peticion asociada al admin
	 * @return
	 */
	public boolean removeUser(HttpServletRequest request);
}