package g8104.business.contact;

import javax.servlet.http.HttpServletRequest;

public interface ContactBusiness {

	/**
	 * Escribe un mensaje en la cola de mensajes
	 * @param mensaje
	 * @param opcion
	 * @param selector
	 */
	public void escrituraJMS(HttpServletRequest request);
	
	
	/**REaliza la lectura de los mensajes de la cola sin borrarlos, en los dos sentidos autor destinatario
	 * @param request Parámetro recibo por el jsp, que contiene los parametros y atributos pertinentes
	 * @return Devuelve los mensajes en forma de String
	 */
	public String lecturaJMS(HttpServletRequest request);
	
	
	/**
	 * Realiza la lectura de los mensajes sin borrarlos, del usuario que ha iniciado sesion
	 * @param request Parámetro recibo por el jsp, que contiene los parametros y atributos pertinentes
	 * @return Devuelve los mensajes en forma de String
	 */
	public String lecturaBuzon(HttpServletRequest request);
	
	
}
