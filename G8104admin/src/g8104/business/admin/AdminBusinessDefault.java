package g8104.business.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import g8104.data.Administrador;
import g8104.data.DataProxy;


public class AdminBusinessDefault implements AdminBusiness {
	@Override
	public boolean loginAdmin(HttpServletRequest request) {
		String password=request.getParameter("password");
		//Se comprueba si usuario existe
		if(!userExists(request.getParameter("email"))) return false;
		
		//Se comprueba si las credenciales son correctas
		Administrador admin = passwordIsCorrect(request.getParameter("email"),password);
		
		//Si son correctas se crea una sesion con el atributo user
		if(admin!=null){
			HttpSession miSesion = request.getSession(true);
			miSesion.setAttribute("user",admin);
			return true;
		}
		else return false;
	}
	
	@Override
	public boolean adminIsLoggedin(HttpServletRequest request){
		//Se determina si el usuario esta logueado, comprobando si tiene una sesion asociada
		HttpSession session = request.getSession(false);
		if(session != null){
			if(session.getAttribute("user") != null)return true;
			else return false;
		}
		else{
			return false;
		}
	}
	
	@Override
	public void logout(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		
		if(session != null){
		    session.invalidate();
		}
	}
	
	/**
	 * Proxy to communicate with data
	 */
	private DataProxy dp = new DataProxy();
	
	/**
	 * Comprueba si un usuario ya existe en la base de datos
	 * 
	 * @return true, si el usuario existe en la base de datos; false, si no existe en la base de datos
	 */
	private boolean userExists(String email){
		if(dp.findAdmin(email).isEmpty()) return false;
		else return true;
	}
	
	/**
	 * Comprueba las credenciales de un usuario
	 * 
	 * @param email - email del usuario
	 * @param password - contrasena del usuario
	 * @return el objeto usuario asociado si las credenciales son correctas; null si las credenciales son incorrectas
	 */
	private Administrador passwordIsCorrect(String email,String password){
		List<Administrador> result = dp.findAdmin(email);
		if(result == null) return null;
		
		if(result.get(0).getPassword().equals(password)) return result.get(0);
		else return null;
	}
}