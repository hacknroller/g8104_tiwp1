package g8104.business.admin;

import javax.servlet.http.HttpServletRequest;

public interface AdminBusiness {
	/**
	 * Logea a un usuario, si este existe
	 * 
	 * @param email - Correo electronico del usuario
	 * @param password - Contrasena del usuario
	 * @return true, si se ha logueado al usuario; false, si no se ha logueado al usuario
	 */
	public boolean loginAdmin(HttpServletRequest request);
	
	public boolean adminIsLoggedin(HttpServletRequest request);
	
	/**
	 * Desloguea al admin
	 * 
	 * @param request - Peticion asociada al admin
	 */
	public void logout(HttpServletRequest request);
}