<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Product Details</title>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <title>Product Details | E-Shopper</title>
	    <link href="css/bootstrap.min.css" rel="stylesheet">
	    <link href="css/font-awesome.min.css" rel="stylesheet">
	    <link href="css/prettyPhoto.css" rel="stylesheet">
	    <link href="css/price-range.css" rel="stylesheet">
	    <link href="css/animate.css" rel="stylesheet">
		<link href="css/main.css" rel="stylesheet">
		<link href="css/responsive.css" rel="stylesheet">
	    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
	    <![endif]-->
	    <link rel="shortcut icon" href="images/ico/favicon.ico">
	    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
	</head>
	<body>
		<header id="header"><!--header-->
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="/G8104admin/homePage"><img src="images/home/logo.png" alt="" /></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<li><a href="/G8104admin/logout" class="active"><i class="fa fa-lock"></i> Logout</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->

			<div class="header-bottom"><!--header-bottom-->
				<div class="container">
					<div class="row">
						<div class="col-sm-9">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
							<div class="mainmenu pull-left">
								<ul class="nav navbar-nav collapse navbar-collapse">
									<form method="POST" action="/G8104admin/adminProductsPage">								
										<button type="submit" class="btn btn-default add-to-cart">Volver</button>
									</form>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div><!--/header-bottom-->
	</header><!--/header-->

	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-9 padding-right">
				<h2 class="title text-center">Articulo</h2>
					<div class="product-details"><!--product-details-->
						<div class="col-sm-5">
							<div class="view-product">
								<img src="<c:url value="http://localhost:8080/images/${product.getPicture()}"/>" alt="" />
							</div>
						</div>
						<div class="col-sm-7">
							<div class="product-information"><!--/product-information-->
								<h2>${product.getTitle()}</h2>
								<span>
									<span>${product.getPrice()}&euro;</span>
								</span>
								<p><b>Categoria:</b> ${product.getCategory()}</p>
								<p><b>Estado del producto:</b> ${product.getState()}</p>
								<p><b>Vendedor: </b>${product.getUsuario().getName()} ${product.getUsuario().getFirstsurname()}</p>
								<p><b>Ubicaci&oacute;n: </b> ${product.getUsuario().getCity()}</p>
							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->

					<div class="category-tab shop-details-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#reviews" data-toggle="tab">Descripci&oacute;n del Producto</a></li>
							</ul>
						</div>
						<div class="tab-content">
							<div class="tab-pane fade active in" id="reviews" >
								<div class="col-sm-12">
									<p>${product.getDescription()}</p>
								</div>
							</div>
						</div>
					</div><!--/category-tab-->
				</div>
				
				<div class="col-sm-3">
					<h2 class="title text-center">Modificar articulo</h2>
					<div class="signup-form"><!--sign up form-->
						<form method="POST" action="/G8104admin/modifyProduct" enctype="multipart/form-data">
							<input type="text" name="titleChange" placeholder="T&iacute;tulo" />
							<select name="estadoChange">
								<option selected disabled hidden value="">Estado</option>
								<option value="Disponible">Disponible</option>
								<option value="Reservado">Reservado</option>
								<option value="Vendido">Vendido</option>
							</select>
							<select name="categoriaChange">
								<option selected disabled hidden value="">Categoria</option>
								<option value="Tecnologia">Tecnologia</option>
								<option value="Ropa">Ropa</option>
								<option value="Otros">Otros</option>
							</select>
							<textarea id="areadetexto" name="descriptionChange" placeholder="Descripci&oacute;n"></textarea>
							<input type="text" name="priceChange" placeholder="Precio"/>
							
							<input type="file" name="file" id="file" class="inputfile" data-multiple-caption="{count} files selected"/>
							<label for="file" class="btn btn-default" ><span>Imagen del producto</span></label>
							
							<input type="hidden" name="productToModify" value="<c:url value="${product.getIdproducto()}"/>"/>
							<button type="submit" class="btn btn-default">Modificar</button>
						</form>
						<br>
						<br>
						<form method="POST" action="/G8104admin/removeProduct">
							<input type="hidden" name="productToModify" value="<c:url value="${product.getIdproducto()}"/>"/>
							<button type="submit" class="btn btn-default add-to-cart">Eliminar</button>
						</form>
					</div><!--/sign up form-->				
				</div>
			</div>
		</div>
	</section>

	<footer id="footer"><!--Footer-->
	</footer><!--/Footer-->



    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
	</body>
</html>