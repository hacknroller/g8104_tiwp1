package g8104.servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.actions.*;

/**
 * Servlet central que controla todo el trafico de la aplicacion
 */
@WebServlet(name = "FrontControllerServlet",
			urlPatterns ={"/homePage",
					"/loginPage",
					"/signup",
					"/login",
					"/logout",
					"/accountPage",
					"/modifyUser",
					"/productDetailsPage",
					"/myProductsPage",
					"/modifyProductPage",
					"/createProduct",
					"/removeProduct",
					"/removeUser" ,
					"/contactUser", 
					"/chatWrite", 
					"/removeUser",
					"/modifyProduct",
					"/searchPage",
					"/advancedSearch",
					"/chatRead",
					"/buzonEntrada",
					"/aboutUsPage"})

@MultipartConfig
public class FrontControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private HashMap<String, Action> GEThandlerHash = new HashMap<String, Action>();
	private HashMap<String, Action> POSThandlerHash = new HashMap<String, Action>();

	/**
	 * Metodo init
	 */
	public void init(ServletConfig config) throws ServletException {
		GEThandlerHash.put("/homePage", new HomePage());
		GEThandlerHash.put("/loginPage", new LoginPage());
		GEThandlerHash.put("/logout", new Logout());
		GEThandlerHash.put("/myProductsPage", new MyProductsPage());
		GEThandlerHash.put("/accountPage", new AccountPage());
		GEThandlerHash.put("/advancedSearch", new AdvancedSearchPage());
		GEThandlerHash.put("/aboutUsPage", new AboutUsPage());
		POSThandlerHash.put("/signup", new Signup());
		POSThandlerHash.put("/login", new Login());
		POSThandlerHash.put("/modifyUser", new ModifyUser());
		POSThandlerHash.put("/createProduct", new CreateProduct());
		POSThandlerHash.put("/productDetailsPage", new ProductDetailsPage());
		POSThandlerHash.put("/modifyProductPage", new ModifyProductPage());
		POSThandlerHash.put("/removeUser", new RemoveUser());
		POSThandlerHash.put("/contactUser", new ContactUser());
		POSThandlerHash.put("/chatWrite", new ChatWrite());
		POSThandlerHash.put("/chatRead", new ChatRead());
		POSThandlerHash.put("/modifyProduct", new ModifyProduct());
		POSThandlerHash.put("/searchPage", new SearchPage());
		POSThandlerHash.put("/removeProduct", new RemoveProduct());
		POSThandlerHash.put("/buzonEntrada", new BuzonEntrada());
	}

	/**
	 * Metodo doGet
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se obtiene la clase que implementa la accion
		Action requestHandler = (Action) GEThandlerHash.get(request.getServletPath());
		
		//Si el servicio no existe
		if(requestHandler == null){
			//Se lanza error 404
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
		else{
			//Si el servicio existe se ejecuta la accion que lo gestiona
			String url = requestHandler.handleRequest(request, response);
			
			//Se redirecciona a la vista adecuada
			if(url == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND);				
			}
			else{
				request.getRequestDispatcher(url).forward(request, response);
			}
		}
	}

	/**
	 * Metodo doPost
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Se obtiene la clase que implementa la accion
		Action requestHandler = (Action) POSThandlerHash.get(request.getServletPath());
		
		//Si el servicio no existe
		if(requestHandler == null){
			//Se lanza error 404
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
		}
		else{
			//Si el servicio existe se ejecuta la accion que lo gestiona
			String url = requestHandler.handleRequest(request, response);
			
			//Se redirecciona a la vista adecuada
			if(url == null){
				response.sendError(HttpServletResponse.SC_NOT_FOUND);				
			}
			else{
				request.getRequestDispatcher(url).forward(request, response);
			}
		}
	}
}