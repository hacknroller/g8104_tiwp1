package g8104.business.product;

import java.io.IOException;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import g8104.data.DataProxy;
import g8104.data.Producto;
import g8104.data.Usuario;

public class ProductBusinessDefault implements ProductBusiness {

	/**
	 * Proxy to communicate with data
	 */
	private DataProxy dp = new DataProxy();
	
	@Override
	public boolean createProduct(HttpServletRequest request) {
		String image="defaultProduct.jpg";
		
		//Se obtiene el ultimo id de un producto registrado
		Integer lastID = dp.lastIDProduct();
		String newID = (lastID + 1) + "";
		
		//Se hace upload de la imagen
		try {
			image = new g8104.business.image.ImageBusinessDefault().uploadImage(request, newID);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (ServletException e) {
			e.printStackTrace();
			return false;
		} catch (NamingException e) {
			e.printStackTrace();
			return false;
		}

		//Se inserta en la base de datos
		dp.insertProduct(request.getParameter("title"),request.getParameter("price"),request.getParameter("description"),image,request.getParameter("categoria"),(Usuario) request.getSession().getAttribute("user"));
		
		return true;
	}

	@Override
	public List<Producto> find6MostRecentProduct() {
		
		List<Producto> result = dp.find6MostRecentProducts();
		return result;
	}

	@Override
	public List<Producto> findProductsByUser(HttpServletRequest request) {
		//Propietario de los productos
		Usuario owner = (Usuario) request.getSession(false).getAttribute("user");
		if(owner == null)
			return null;
		
		//Se buscan sus productos
		List<Producto> result = dp.findProductsByUser(owner.getEmail());
		return result;
	}
	
	@Override
	public List<Producto> findProductById(HttpServletRequest request) {
		//Se obtiene el id del producto
		String id = request.getParameter("productToExplore");
		if(id == null)
			return null;
		
		//Se obtiene el producto
		List<Producto> result = dp.findProductById(Integer.parseInt(id));
		return result;
	}
	
	@Override
	public Producto modifyProduct(HttpServletRequest request) {
		//Se obtiene el producto a modificar
		List<Producto> productChanges = dp.findProductById(Integer.parseInt(request.getParameter("productToModify")));
		System.out.println("----------------------> "+request.getParameter("estadoChange"));
		if(!request.getParameter("titleChange").equals("")) productChanges.get(0).setTitle(request.getParameter("titleChange"));
		if(request.getParameter("estadoChange") != null) productChanges.get(0).setState(request.getParameter("estadoChange"));
		if(request.getParameter("categoriaChange") != null) productChanges.get(0).setCategory(request.getParameter("categoriaChange"));
		if(!request.getParameter("descriptionChange").equals("")) productChanges.get(0).setDescription(request.getParameter("descriptionChange"));
		if(!request.getParameter("priceChange").equals("")) productChanges.get(0).setPrice(request.getParameter("priceChange"));
		
		//Update image
		try {
			if(!request.getPart("file").getSubmittedFileName().equals("")){
				String image="defaultProduct.jpg";
				try {
					image = new g8104.business.image.ImageBusinessDefault().uploadImage(request, productChanges.get(0).getIdproducto()+"");
				} catch (IOException e) {
					e.printStackTrace();
					return null;
				} catch (ServletException e) {
					e.printStackTrace();
					return null;
				} catch (NamingException e) {
					e.printStackTrace();
					return null;
				}
				productChanges.get(0).setPicture(image);			
			}
		} catch (IOException | ServletException e) {
			e.printStackTrace();
		}

		//Se retorna el producto actualizado
		Producto newProduct = dp.updateProduct(productChanges.get(0));
		return newProduct;
	}
	
	@Override
	public boolean removeProduct(HttpServletRequest request) {
		//Se obtiene el producto a modificar
		List<Producto> products = dp.findProductById(Integer.parseInt(request.getParameter("productToExplore")));
		Producto productDelete=products.get(0);
		int idDelete=productDelete.getIdproducto();
		dp.removeProduct(idDelete);
		return true;
	}

	@Override
	public List<Producto> findProductsByTitleAndDescription(HttpServletRequest request) {
		request.getParameter("titleAndDescription");
		
		List<Producto> result = dp.findByTitleAndDescription(request.getParameter("titleAndDescription"));
		
		return result;
	}

	@Override
	public List<Producto> advancedSearch(HttpServletRequest request) {
		String title = "";
		String description = "";
		String category = "";
		String city = "";
		String user = "";
		
		if(!request.getParameter("title").equals("")) title = request.getParameter("title");
		if(!request.getParameter("description").equals("")) description = request.getParameter("description");
		if(request.getParameter("categoria") != null) category = request.getParameter("categoria");
		if(!request.getParameter("ciudad").equals("")) city = request.getParameter("ciudad");
		if(!request.getParameter("vendedor").equals("")) user = request.getParameter("vendedor");
		
		List<Producto> result = dp.advancedSearch(title,description,category,city,user);
		
		return result;
	}
}