package g8104.business.product;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import g8104.data.Producto;

public interface ProductBusiness {
	public boolean createProduct(HttpServletRequest request);
	
	public List<Producto> find6MostRecentProduct();
	
	public List<Producto> findProductsByUser(HttpServletRequest request);
	
	public List<Producto> findProductById(HttpServletRequest request);
	
	public Producto modifyProduct(HttpServletRequest request);
	
	public boolean removeProduct(HttpServletRequest request);
	
	public List<Producto> findProductsByTitleAndDescription(HttpServletRequest request);
	
	public List<Producto> advancedSearch(HttpServletRequest request);
}