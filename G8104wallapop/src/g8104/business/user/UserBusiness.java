package g8104.business.user;

import javax.servlet.http.HttpServletRequest;

public interface UserBusiness {
	/**
	 * Registra a un usuario, si este no existe
	 * 
	 * @param name - Nombre del usuario
	 * @param password - Contrasena del usuario
	 * @param email - Correo electronico del usuario
	 * @return true, si se ha registrado al usuario; false, si no se ha registrado al usuario
	 */
	public boolean signupUser(HttpServletRequest request);
	
	/**
	 * Logea a un usuario, si este existe
	 * 
	 * @param email - Correo electronico del usuario
	 * @param password - Contrasena del usuario
	 * @return true, si se ha logueado al usuario; false, si no se ha logueado al usuario
	 */
	public boolean loginUser(HttpServletRequest request);
	
	/**
	 * Comprueba si un usuario esta logueado en la aplicacion
	 * 
	 * @param request - Peticion asociada al usuario
	 * @return true, si el usuario esta logueado; false, si no esta logueado
	 */
	public boolean userIsLoggedin(HttpServletRequest request);
	
	/**
	 * Desloguea al usuario
	 * 
	 * @param request - Peticion asociada al usuario
	 */
	public void logout(HttpServletRequest request);
	
	/**
	 * Modifica los datos del usuario deseados
	 * 
	 * @param request - Peticion asociada al usuario
	 * @return
	 */
	public boolean modifyUser(HttpServletRequest request);
	
	/**
	 * Elimina al usuario
	 * 
	 * @param request - Peticion asociada al usuario
	 * @return
	 */
	public boolean removeUser(HttpServletRequest request);
}