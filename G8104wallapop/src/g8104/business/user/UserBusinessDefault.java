package g8104.business.user;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import g8104.data.DataProxy;
import g8104.data.Usuario;

public class UserBusinessDefault implements UserBusiness {

	@Override
	public boolean signupUser(HttpServletRequest request) {
		String password=request.getParameter("password");
		String confirmpsw=request.getParameter("confirmpsw");
		//Se comprueba si el usuario existe y se comprueba si la contraseña y su confirmacion son iguales
		if(userExists(request.getParameter("email")) || !password.equals(confirmpsw)) return false;
		//Si existe, se inserta en la base de datos
		insertUser(request.getParameter("name"), request.getParameter("firstsurname"), request.getParameter("secondsurname"),request.getParameter("email"),
				   request.getParameter("city"),cifrarPsw(password));
		return true;
	}
	
	@Override
	public boolean loginUser(HttpServletRequest request) {
		String password=request.getParameter("password");
		String pswCifrado=cifrarPsw(password);
		//Se comprueba si usuario existe
		if(!userExists(request.getParameter("email"))) return false;
		
		//Se comprueba si las credenciales son correctas
		Usuario user = passwordIsCorrect(request.getParameter("email"),pswCifrado);
		
		//Si son correctas se crea una sesion con el atributo user
		if(user!=null){
			HttpSession miSesion = request.getSession(true);
			miSesion.setAttribute("user",user);
			return true;
		}
		else return false;
	}
	
	@Override
	public boolean userIsLoggedin(HttpServletRequest request){
		//Se determina si el usuario esta logueado, comprobando si tiene una sesion asociada
		HttpSession session = request.getSession(false);
		if(session != null){
			if(session.getAttribute("user") != null)return true;
			else return false;
		}
		else{
			return false;
		}
	}
	
	@Override
	public void logout(HttpServletRequest request) {
		HttpSession session = request.getSession(false);
		
		if(session != null){
		    session.invalidate();
		}
	}
	
	@Override
	public boolean modifyUser(HttpServletRequest request) {
		boolean result = true;
		//Se obtiene el atributo usuario, y se guarda el antiguo email en caso de que sea cambiado
		Usuario userChanges = (Usuario) request.getSession(false).getAttribute("user");
		String email = userChanges.getEmail();

		//Se comprueban los campos a actualizar
		if(!request.getParameter("nameChange").equals("")) userChanges.setName(request.getParameter("nameChange"));
		if(!request.getParameter("firstsurnameChange").equals("")) userChanges.setFirstsurname(request.getParameter("firstsurnameChange"));
		if(!request.getParameter("secondsurnameChange").equals("")) userChanges.setSecondsurname(request.getParameter("secondsurnameChange"));
		if(!request.getParameter("ciudad").equals("")) userChanges.setCity(request.getParameter("ciudad"));
		
		//Si se cambia la contrasena, la antigua debe coincidir
		String passCifrada = cifrarPsw(request.getParameter("oldPasswordChange"));
		if(passCifrada.equals(userChanges.getPassword()) && !request.getParameter("newPasswordChange").equals(""))
			userChanges.setPassword(cifrarPsw(request.getParameter("newPasswordChange")));
		else if (!cifrarPsw(request.getParameter("oldPasswordChange")).equals(userChanges.getPassword()) && !request.getParameter("newPasswordChange").equals(""))
			result = false;

		//Se actualizan los valores en la base de datos
		Usuario newUser = dp.updateUser(userChanges, email);
		
		//Se desloguea al usuario
		logout(request);
		
		//Se le crea una nueva sesion de login
		HttpSession miSesion = request.getSession(true);
		miSesion.setAttribute("user",newUser);
		
		return result;
	}
	
	@Override
	public boolean removeUser(HttpServletRequest request) {
		//obtiene el email del usuario de sesion
		Usuario user = (Usuario) request.getSession(false).getAttribute("user");
		String sessionEmail = user.getEmail();
		//obtiene la contrase�a del usuario de sesion
		String sessionPassword = user.getPassword();
		//obtiene la contrase�a introducida por el usuario
		String requestPassword=request.getParameter("passwordDelete");
		//hash de la contrase�a del request
		String hashRequestPassword=cifrarPsw(requestPassword);
		if(sessionPassword.equals(hashRequestPassword)==false) return false;
		//si las contrasenias coinciden se elimina la cuenta
		dp.removeUser(sessionEmail);
		//Se desloguea al usuario
		logout(request);
		return true;
	}
	
	/**
	 * Proxy to communicate with data
	 */
	private DataProxy dp = new DataProxy();
	
	/**
	 * Comprueba si un usuario ya existe en la base de datos
	 * 
	 * @return true, si el usuario existe en la base de datos; false, si no existe en la base de datos
	 */
	private boolean userExists(String email){
		if(dp.findUser(email).isEmpty()) return false;
		else return true;
	}
	
	/**
	 * Introduce un usuario en la base de datos
	 * 
	 * @param name
	 * @param email
	 * @param password
	 */
	private void insertUser(String name, String firstsurname, String secondsurname, String email, String city, String password){
		//Se crea un objeto con las credenciales del usuario
		//String passwordCifrado=cifrarPsw(password);
		Usuario newUser = new Usuario();
		newUser.setName(name);
		newUser.setFirstsurname(firstsurname);
		newUser.setSecondsurname(secondsurname);
		newUser.setEmail(email);
		newUser.setCity(city);
		newUser.setPassword(password);
		
		//Se inserta en la base de datos
		dp.insertUser(newUser);
	}
	
	/**
	 * Comprueba las credenciales de un usuario
	 * 
	 * @param email - email del usuario
	 * @param password - contrasena del usuario
	 * @return el objeto usuario asociado si las credenciales son correctas; null si las credenciales son incorrectas
	 */
	private Usuario passwordIsCorrect(String email,String password){
		List<Usuario> result = dp.findUser(email);
		if(result == null) return null;
		
		if(result.get(0).getPassword().equals(password)) return result.get(0);
		else return null;
	}
	
	/**
	 * Devuelve la contraseña cifrada con hash md5
	 * 
	 * @param contraseña
	 * @return contraseña cifrada
	 */
   private String cifrarPsw(String contrasena) {
        String generatedPassword = null;
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(contrasena.getBytes());
            //Get the hash's bytes 
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            generatedPassword = sb.toString();
        } 
        catch (NoSuchAlgorithmException e) 
        {
            e.printStackTrace();
        }
        return generatedPassword;
    }
}