package g8104.business.contact;

import java.util.*;

public class InformacionProperties {

	private static String strQCF;

	private static String strQueue;

	private static String strQueueAsincrona;

	// **************************************************
	public InformacionProperties() {
		super();
	}

	// **************************************************
	public static String getQCF() {

		if (strQCF == null)
			cagarProperties();

		return strQCF;
	}

	// **************************************************
	public static String getQueue() {

		if (strQueue == null)
			cagarProperties();

		return strQueue;
	}

	// **************************************************
		public static String getQueueAsincrona() {

			if (strQueueAsincrona == null)
				cagarProperties();

			return strQueueAsincrona;
		}

	// **************************************************
	private static void cagarProperties() throws MissingResourceException {

//		PropertyResourceBundle appProperties = null;

		try {

//			appProperties = (PropertyResourceBundle) PropertyResourceBundle
//					.getBundle(nombreProperties);

			strQCF = "jms/cf1.1";
			strQueue = "jms/queue1.1";

		} catch (MissingResourceException e) {

			throw e;
		}

	}
}