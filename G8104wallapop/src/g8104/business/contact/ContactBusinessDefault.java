package g8104.business.contact;



import java.util.Enumeration;


import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.servlet.http.HttpServletRequest;
import g8104.data.Usuario;



public class ContactBusinessDefault implements ContactBusiness {

	private javax.jms.ConnectionFactory factory = null;
	private javax.naming.InitialContext contextoInicial = null;
	private javax.jms.Destination cola = null;
	private javax.jms.Connection Qcon = null;
	private javax.jms.Session QSes = null;
	private javax.jms.MessageProducer Mpro = null;
	private javax.jms.MessageConsumer Mcon = null;
	
	public String lecturaJMS(HttpServletRequest request) {
		Usuario user = (Usuario) request.getSession(false).getAttribute("user");
		String emailDestinatario = (String) request.getSession(false).getAttribute("userLector");
		String email = user.getEmail();
		StringBuffer mSB = new StringBuffer(64);
		try {
			contextoInicial = new javax.naming.InitialContext();

			
			factory = (javax.jms.ConnectionFactory) contextoInicial
					.lookup(InformacionProperties.getQCF());
			cola = (javax.jms.Destination) contextoInicial
					.lookup(InformacionProperties.getQueue());
		

			Qcon = factory.createConnection();

			QSes = Qcon
					.createSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);

			String sSelector = "(AUTOR = '" + email + "' AND DESTINATARIO = '" + emailDestinatario + "') OR (AUTOR = '" + emailDestinatario + "' AND DESTINATARIO = '" + email + "')";

			
			Mcon = QSes.createConsumer(cola, sSelector);

			char a = '"';
			Qcon.start();
			while (true) {
				Message mensaje = Mcon.receive(100);
				if (mensaje != null) {
					if (mensaje instanceof TextMessage) {
						TextMessage m = (TextMessage) mensaje;
						mSB.append("<ul><li><i class=" + a + "fa fa-user" + a + "></i>" + " " + m.getStringProperty("AUTOR") +"</a></li>"+
								"</ul><p>" + m.getText() + " </p>");
					} else {
						// JHC ************ No es del tipo correcto
						break;
					}
				} else // NO existe mensaje, mensaje es null
				{
					mSB.append("TIdW 2013-14: No hay Mensajes en la Queue</br>");
					break;
				}

			}
			this.Mcon.close();
			this.QSes.close();
			this.Qcon.close();

		} catch (javax.jms.JMSException e) {
			System.out
					.println(".....JHC *************************************** Error de JMS: "
							+ e.getLinkedException().getMessage());
			System.out
					.println(".....JHC *************************************** Error de JMS: "
							+ e.getLinkedException().toString());
		} catch (Exception e) {
			System.out
					.println("JHC *************************************** Error Exception: "
							+ e.getMessage());
		}
		return mSB.toString();
	}
	
	
	
	@Override
	public void escrituraJMS(HttpServletRequest request) {
		String mensaje = (String) request.getParameter("mensaje");
		String emailDestinatario = (String) request.getSession(false).getAttribute("userLector");
		System.out.println("Escribimos mensaje..."  +  " Mensaje: " + mensaje);
		
		try {

			contextoInicial = new javax.naming.InitialContext();

				factory = (javax.jms.ConnectionFactory) contextoInicial
						.lookup(InformacionProperties.getQCF());
				cola = (javax.jms.Destination) contextoInicial
						.lookup(InformacionProperties.getQueue());
			
			Qcon = factory.createConnection();
			QSes = Qcon
					.createSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);

			Mpro = QSes.createProducer(cola);

			javax.jms.TextMessage men = QSes.createTextMessage();
			
			men.setText(mensaje);
			Usuario user = (Usuario) request.getSession(false).getAttribute("user");
			men.setStringProperty("AUTOR", user.getEmail());
			men.setStringProperty("DESTINATARIO", emailDestinatario);
			Qcon.start();
			Mpro.send(men);

			this.Mpro.close();
			this.QSes.close();
			this.Qcon.close();

		} catch (javax.jms.JMSException e) {
			System.out
					.println(".....JHC *************************************** Error de JMS: "
							+ e.getLinkedException().getMessage());
			System.out
					.println(".....JHC *************************************** Error de JMS: "
							+ e.getLinkedException().toString());
		} catch (Exception e) {
			System.out
					.println("JHC *************************************** Error Exception: "
							+ e.getMessage());
		}

	}

	private static Queue _queue;

	@Override
	public String lecturaBuzon(HttpServletRequest request){
		System.out.println("Escritura en buzon");
		Usuario user = (Usuario) request.getSession(false).getAttribute("user");
		String email = user.getEmail();
		
		char a = '"';
		Connection _connection = null;
		StringBuffer _sB = new StringBuffer(32);
		_sB.append("<br>");
		
		try {
			contextoInicial = new javax.naming.InitialContext();
			factory = (javax.jms.ConnectionFactory) contextoInicial
					.lookup(InformacionProperties.getQCF());
			_queue = (Queue) contextoInicial
					.lookup(InformacionProperties.getQueue());
			
			_connection = factory.createConnection();

			Session session = _connection.createSession(false,
					Session.AUTO_ACKNOWLEDGE);
			String sSelector = "DESTINATARIO='"+ email + "'";
			
			QueueBrowser browser = session.createBrowser(_queue, sSelector);
			
			@SuppressWarnings("rawtypes")
			Enumeration msgs = browser.getEnumeration();
			
			
			while (msgs.hasMoreElements()) {
				Message tempMsg = (Message) msgs.nextElement();
				TextMessage m = (TextMessage) tempMsg;
				String urlCor1 = "/G8104wallapop/contactUser?userLector=" + tempMsg.getStringProperty("AUTOR");
				String form1 = "<form method = " + a + "POST" + a + " action=" + a + urlCor1 + a + "><button type=" + a + "submit" + a +" class=" + a + "btn btn-default" + a +">Abrir Chat</button></form>";
				
				
				_sB.append("<ul><li><i class=" + a + "fa fa-user" + a + "></i>"+ tempMsg.getStringProperty("AUTOR") +"</a>"+form1+"</li>"+
					"</ul><p>" + m.getText() + " </p>");
			}

		} catch (javax.jms.JMSException e) {
			System.out
					.println(".....JHC *************************************** Error de JMS: "
							+ e.getLinkedException().getMessage());
			System.out
					.println(".....JHC *************************************** Error de JMS: "
							+ e.getLinkedException().toString());
		} catch (Exception e) {
			System.out
					.println("JHC *************************************** Error Exception: "
							+ e.getMessage());
		}
		finally {
			if (_connection != null) {
				try {
					_connection.close();
				} catch (JMSException e) {
					System.out
							.println(".....JHC *************************************** Error de JMS: "
									+ e.getLinkedException().getMessage());
					System.out
							.println(".....JHC *************************************** Error de JMS: "
									+ e.getLinkedException().toString());
				}
			}
		}
		return _sB.toString();

	}

}
