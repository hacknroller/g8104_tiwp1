package g8104.data;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the producto database table.
 * 
 */
@Entity
@NamedQueries({
	@NamedQuery(name="lastID", query="SELECT MAX(p.idproducto) FROM Producto p"),
	@NamedQuery(name="findAllProducts", query="SELECT p FROM Producto p ORDER BY p.idproducto DESC"),
	@NamedQuery(name="findRecentProducts", query="SELECT p FROM Producto p ORDER BY p.idproducto DESC"),
	@NamedQuery(name="findProductsByUser", query="SELECT p FROM Producto p WHERE p.usuario.email = ?1 ORDER BY p.idproducto DESC"),
	@NamedQuery(name="findProductById", query="SELECT p FROM Producto p WHERE p.idproducto = ?1"),
	@NamedQuery(name="findProductByTitleAndDescription", query="SELECT p FROM Producto p WHERE p.title LIKE ?1 OR p.description LIKE ?1"),
	@NamedQuery(name="advancedSearch", query="SELECT p FROM Producto p WHERE p.title LIKE ?1 AND p.description LIKE ?2 AND p.category LIKE ?3 AND p.usuario.city LIKE ?4 AND p.usuario.email LIKE ?5")
})
public class Producto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id 
	private int idproducto;

	private String category;

	private String description;

	private String picture;

	private String price;

	private String title;
	
	private String state;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="email")
	private Usuario usuario;

	public Producto() {
	}

	public int getIdproducto() {
		return this.idproducto;
	}

	public void setIdproducto(int idproducto) {
		this.idproducto = idproducto;
	}

	public String getCategory() {
		return this.category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPicture() {
		return this.picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getPrice() {
		return this.price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public String getState() {
		return this.state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
}