package g8104.data;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the usuario database table.
 * 
 */
@Entity
@NamedQuery(name="findUser",query="SELECT u FROM Usuario u WHERE u.email=?1")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String email;

	private String city;

	private String firstsurname;

	private String name;

	private String password;

	private String secondsurname;

	//bi-directional many-to-one association to Producto
	@OneToMany(mappedBy="usuario")
	private List<Producto> productos;

	public Usuario() {
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getFirstsurname() {
		return this.firstsurname;
	}

	public void setFirstsurname(String firstsurname) {
		this.firstsurname = firstsurname;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSecondsurname() {
		return this.secondsurname;
	}

	public void setSecondsurname(String secondsurname) {
		this.secondsurname = secondsurname;
	}

	public List<Producto> getProductos() {
		return this.productos;
	}

	public void setProductos(List<Producto> productos) {
		this.productos = productos;
	}

	public Producto addProducto(Producto producto) {
		getProductos().add(producto);
		producto.setUsuario(this);

		return producto;
	}

	public Producto removeProducto(Producto producto) {
		getProductos().remove(producto);
		producto.setUsuario(null);

		return producto;
	}

}