package g8104.data;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DataProxy {
	/**
	 * Busca a un usario en la base de datos por su clave primaria
	 * 
	 * @param email - String con email del usuario
	 * @return La instancia del usuario, y null si no existe
	 */
	@SuppressWarnings("unchecked")
	public List<Usuario> findUser(String email){
		EntityManager em = createConnection();
		
		List<Usuario> result =  em.createNamedQuery("findUser").setParameter(1, email).getResultList();
		
		closeConnection(em);
		
		return result;
	}
	
	public boolean insertUser(Usuario user){
		EntityManager em = createConnection();
		
		persistUser(em,user);
		commit(em);
		
		closeConnection(em);
		
		return true;
	}
	
	public Usuario updateUser(Usuario userChanges, String email){
		EntityManager em = createConnection();
		
		//Se busca al usuario que desea cambiar sus credenciales en la base de datos
		Usuario newUser = em.find(Usuario.class,email);
				
		//Se actualizan sus valores
		newUser.setName(userChanges.getName());
		newUser.setFirstsurname(userChanges.getFirstsurname());
		newUser.setSecondsurname(userChanges.getSecondsurname());
		newUser.setCity(userChanges.getCity());
		newUser.setPassword(userChanges.getPassword());
		
		//Se comitea resultados
		commit(em);
		
		closeConnection(em);
		
		return newUser;
	}
	
	/**
	 * Elimina a un usario en la base de datos por su clave primaria
	 * 
	 * @param email - String con email del usuario
	 * @return true si ha sido eliminado y false si no.
	 */
	public boolean removeUser(String email){
		EntityManager em = createConnection();
		Usuario usuario=em.find(Usuario.class, email);
			em.remove(usuario);
			//Se comitea resultados
			commit(em);
		  closeConnection(em);
		  return true;
	}
	
	public boolean insertProduct(String title, String price, String description, String image, String categoria, Usuario user){
		
		Producto newProduct = new Producto();
		newProduct.setTitle(title);
		newProduct.setPrice(price);
		newProduct.setDescription(description);
		newProduct.setUsuario(user);
		newProduct.setCategory(categoria);
		newProduct.setPicture(image);
		newProduct.setState("Disponible");
		
		EntityManager em = createConnection();
		
		persistProduct(em,newProduct);
		commit(em);
		
		closeConnection(em);
		return true;
	}
	
	@SuppressWarnings("unchecked")
	public Integer lastIDProduct(){
		EntityManager em = createConnection();
		
		List<Integer> result =  em.createNamedQuery("lastID").getResultList();
		
		Integer id = 0;
		
		if(result.get(0) != null)
			id = result.get(0);
		
		return id;
	}
	
	@SuppressWarnings("unchecked")
	public List<Producto> find6MostRecentProducts(){
		EntityManager em = createConnection();
		
		List<Producto> result =  em.createNamedQuery("findRecentProducts").setMaxResults(6).getResultList();
		
		closeConnection(em);
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Producto> findProductsByUser(String email){
		EntityManager em = createConnection();
		
		List<Producto> result =  em.createNamedQuery("findProductsByUser").setParameter(1, email).getResultList();
		
		closeConnection(em);
		
		return result;
	}	
	
	@SuppressWarnings("unchecked")
	public List<Producto> findProductById(int id){
		EntityManager em = createConnection();
		
		List<Producto> result =  em.createNamedQuery("findProductById").setParameter(1, id).getResultList();
		
		closeConnection(em);
		
		return result;
	}
	
	public Producto updateProduct(Producto productoChanges){
		EntityManager em = createConnection();
		
		//Se busca al usuario que desea cambiar sus credenciales en la base de datos
		Producto newProduct = em.find(Producto.class,productoChanges.getIdproducto());
				
		//Se actualizan sus valores
		newProduct.setTitle(productoChanges.getTitle());
		newProduct.setState(productoChanges.getState());
		newProduct.setCategory(productoChanges.getCategory());
		newProduct.setDescription(productoChanges.getDescription());
		newProduct.setPrice(productoChanges.getPrice());
		newProduct.setPicture(productoChanges.getPicture());
		
		//Se comitea resultados
		commit(em);
		
		closeConnection(em);
		
		return newProduct;
	}
	
	/**
	 * Elimina a un producto en la base de datos por su clave primaria
	 * 
	 * @param email - String con email del usuario
	 * @return true si ha sido eliminado y false si no.
	 */
	public boolean removeProduct(int idProducto){
		EntityManager em = createConnection();
		Producto producto=em.find(Producto.class, idProducto);
			em.remove(producto);
			//Se comitea resultados
			commit(em);
		  closeConnection(em);
		  return true;
	}
	
	@SuppressWarnings("unchecked")
	public List<Producto> findByTitleAndDescription(String text){
		EntityManager em = createConnection();
		
		List<Producto> result =  em.createNamedQuery("findProductByTitleAndDescription").setParameter(1, "%"+text+"%").getResultList();
		
		closeConnection(em);
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Producto> advancedSearch(String title,String description,String category,String city,String user){
		EntityManager em = createConnection();
		
		List<Producto> result =  em.createNamedQuery("advancedSearch").setParameter(1, "%"+title+"%").setParameter(2, "%"+description+"%")
				.setParameter(3, "%"+category+"%").setParameter(4, "%"+city+"%").setParameter(5, "%"+user+"%").getResultList();
		
		closeConnection(em);
		
		return result;
	}
	
	/**
	 * Crea una conexion con la base de datos e inicia una transaccion
	 * 
	 * @return objeto EntityManager asociado
	 */
	private EntityManager createConnection(){
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("G8104wallapop");
		EntityManager em = factory.createEntityManager();
		em.getTransaction().begin();
		
		return em;
	}
	
	/**
	 * Cierra una conexion con la base de datos
	 * @param em - objeto EntityManager asociado
	 */
	private void closeConnection(EntityManager em){
		em.close();
	}
	
	/**
	 * Hace persistente un usuario en la entidad
	 * @param em - objeto EntityManager asociado
	 * @param user - Usuario a persistir
	 */
	private void persistUser(EntityManager em, Usuario user){
		em.persist(user);
	}
	
	/**
	 * Hace persistente un producto en la entidad
	 * @param em - objeto EntityManager asociado
	 * @param user - Producto a persistir
	 */
	private void persistProduct(EntityManager em, Producto product){
		em.persist(product);
	}
	
	/**
	 * Comitea en la base de datos
	 * @param em - objeto EntityManager asociado
	 */
	private void commit(EntityManager em){
		em.getTransaction().commit();
	}
}