package g8104.actions;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.business.product.ProductBusinessDefault;
import g8104.business.user.UserBusinessDefault;
import g8104.data.Producto;

public class SearchPage implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Si no esta logueado se redirecciona a login
		if(!new UserBusinessDefault().userIsLoggedin(request)) return new LoginPage().handleRequest(request, response);
		
		List<Producto> requiredProducts = new ProductBusinessDefault().findProductsByTitleAndDescription(request);
		
		System.out.println("------------------>"+request.getParameter("titleAndDescription"));
		System.out.println(request.getParameter("numberPageProduct"));
		
		if(!requiredProducts.isEmpty()){
			request.setAttribute("requiredProducts", requiredProducts);
			
			//Numero de paginas de productos producidas
			int numberOfPages = (int) (Math.ceil(requiredProducts.size()/4.0));
			request.setAttribute("numberOfPages", numberOfPages);
			
			//Seleccionar pagina de productos a mostrar, y cual es el primer producto de esa pagina
			Integer firstProduct;
			String numberPageProductStr = request.getParameter("numberPageProduct");
			if(numberPageProductStr == null || numberPageProductStr.length() > 10) firstProduct = 0;
			else{
				if(Integer.parseInt(numberPageProductStr)<1 || Integer.parseInt(numberPageProductStr)>numberOfPages) firstProduct=0;
				else firstProduct = (Integer.parseInt(numberPageProductStr)-1)*4;
			}
			
			request.setAttribute("firstProduct", firstProduct);
			request.setAttribute("titleAndDescription", request.getParameter("titleAndDescription"));
		}
		else request.setAttribute("noProductsFoundMessage", "No se ha encontrado ningun resultado");
		
		
		
		return "search.jsp";
	}
}