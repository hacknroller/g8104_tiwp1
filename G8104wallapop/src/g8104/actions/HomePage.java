package g8104.actions;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.business.product.ProductBusinessDefault;
import g8104.data.Producto;

public class HomePage implements Action {
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Lista de los 6 productos mas recientes
		List<Producto> currentProducts = new ProductBusinessDefault().find6MostRecentProduct();
		request.setAttribute("currentProducts", currentProducts);

		return "index.jsp";
	}
}