package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.business.user.UserBusinessDefault;

public class LoginPage implements Action {
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Si esta logueado se redirige a home
		if(new UserBusinessDefault().userIsLoggedin(request)) return new HomePage().handleRequest(request, response);
		
		return "login.jsp";
	}
}