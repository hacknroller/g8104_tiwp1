package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.business.user.UserBusinessDefault;

public class Logout implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Si tenia una sesion se desloguea
		new UserBusinessDefault().logout(request);

		return new HomePage().handleRequest(request, response);
	}
}