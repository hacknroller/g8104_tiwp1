package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ContactUser implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getSession(false).setAttribute("userLector", request.getParameter("userLector"));
		System.out.println(request.getParameter("userLector"));
		return "contact.jsp";
	}

}
