package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.business.user.UserBusinessDefault;

public class Signup implements Action {
	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Si se consigue registrar, se loguea y se redirige al indice
		if(new UserBusinessDefault().signupUser(request)){
			new UserBusinessDefault().loginUser(request);
			return "index.jsp";
		}
			
		//Si no, se redirige otra vez a la pagina de login
		else{
			request.setAttribute("signupFail", "Ya existe un usuario registrado con el email indicado o las contraseñas no coinciden.");
			return "login.jsp";
		}
	}
}