package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.business.user.UserBusinessDefault;

public class ModifyUser implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(new UserBusinessDefault().modifyUser(request)) return "index.jsp";
		else{
			request.setAttribute("changeFailMessage", "Las password introducidas no coinciden");
			return "account.jsp";
		}
	}
}