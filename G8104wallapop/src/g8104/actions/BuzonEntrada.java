package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.business.contact.ContactBusinessDefault;

public class BuzonEntrada implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String strAux = "";
		
		ContactBusinessDefault mq =new ContactBusinessDefault();
		strAux=mq.lecturaBuzon(request);
		System.out.println("Leemos mensajes de buzon..." + " Mensajes: " + strAux);
		request.setAttribute("mensajes",strAux);
		return "account.jsp";
	}
}
