package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.business.contact.ContactBusinessDefault;

public class ChatRead implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String strAux = "";
		
		ContactBusinessDefault mq =new ContactBusinessDefault();
		strAux=mq.lecturaJMS(request);
		request.setAttribute("mensajes",strAux);
		return "contact.jsp";
	}

}
