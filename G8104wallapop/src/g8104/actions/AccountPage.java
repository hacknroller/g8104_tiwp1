package g8104.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import g8104.business.user.UserBusinessDefault;

public class AccountPage implements Action {

	@Override
	public String handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//Si esta logueado se le permite
		if(new UserBusinessDefault().userIsLoggedin(request)) return "account.jsp";
		else return new HomePage().handleRequest(request, response);
	}
}