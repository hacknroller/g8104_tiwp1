<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Product Details</title>
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <title>Product Details | E-Shopper</title>
	    <link href="css/bootstrap.min.css" rel="stylesheet">
	    <link href="css/font-awesome.min.css" rel="stylesheet">
	    <link href="css/prettyPhoto.css" rel="stylesheet">
	    <link href="css/price-range.css" rel="stylesheet">
	    <link href="css/animate.css" rel="stylesheet">
		<link href="css/main.css" rel="stylesheet">
		<link href="css/responsive.css" rel="stylesheet">
	    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
	    <![endif]-->
	    <link rel="shortcut icon" href="images/ico/favicon.ico">
	    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
	</head>
	<body>
		<c:set var="urlCor1" value="/G8104wallapop/contactUser?userLector="/>
		<c:set var="url" value="${urlCor1}${product.getUsuario().getEmail()}"/>
		
		<header id="header"><!--header-->
		<div class="header-middle"><!--header-middle-->
			<div class="container">
				<div class="row">
					<div class="col-sm-4">
						<div class="logo pull-left">
							<a href="/G8104wallapop/homePage"><img src="images/home/logo.png" alt="" /></a>
						</div>
					</div>
					<div class="col-sm-8">
						<div class="shop-menu pull-right">
							<ul class="nav navbar-nav">
								<%if(session.getAttribute("user") == null){%>
									<li><a href="/G8104wallapop/loginPage" class="active"><i class="fa fa-lock"></i> Login</a></li>
								<%}else{%>
									<jsp:useBean id="user" class="g8104.data.Usuario" scope="session" />
									<li><a href="/G8104wallapop/accountPage"><i class="fa fa-user"></i> <jsp:getProperty property="name" name="user"/> <jsp:getProperty property="firstsurname" name="user"/></a></li>
									<li><a href="/G8104wallapop/myProductsPage"><i class="fa fa-star"></i> Mis productos</a></li>
									<li><a href="/G8104wallapop/advancedSearch"><i class="fa fa-crosshairs"></i> Busqueda Avanzada</a></li>
									<li><a href="/G8104wallapop/logout" class="active"><i class="fa fa-lock"></i> Logout</a></li>
								<%}%>	
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-middle-->

		<div class="header-bottom"><!--header-bottom-->
			<div class="container">
				<div class="row">
					<div class="col-sm-9">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</div>
						<div class="mainmenu pull-left">
							<ul class="nav navbar-nav collapse navbar-collapse">
								<li><a href="/G8104wallapop/homePage">Home</a></li>
								<li class="dropdown"><a href="/G8104wallapop/aboutUsPage">Contacto</a>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="search_box pull-right">
							<form method="POST" action="/G8104wallapop/searchPage" class="searchform">
								<input type="text" maxlength="50" name="titleAndDescription" placeholder="Search" required/>
								<button type="submit" class="btn btn-default"><img src="images/home/searchicon.png"/></button>
							</form>	
						</div>
					</div>
				</div>
			</div>
		</div><!--/header-bottom-->
	</header><!--/header-->

	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
					</div>
				</div>

				<div class="col-sm-9 padding-right">
					<div class="product-details"><!--product-details-->
						<div class="col-sm-5">
							<div class="view-product">
								<img src="<c:url value="http://localhost:8080/images/${product.getPicture()}"/>" alt="" />
							</div>
							<div id="similar-product" class="carousel slide" data-ride="carousel">

								  <!-- Wrapper for slides -->
								    <div class="carousel-inner">
										<div class="item active">
										  <a href=""><img src="images/product-details/similar1.jpg" alt=""></a>
										  <a href=""><img src="images/product-details/similar2.jpg" alt=""></a>
										  <a href=""><img src="images/product-details/similar3.jpg" alt=""></a>
										</div>
										<div class="item">
										  <a href=""><img src="images/product-details/similar1.jpg" alt=""></a>
										  <a href=""><img src="images/product-details/similar2.jpg" alt=""></a>
										  <a href=""><img src="images/product-details/similar3.jpg" alt=""></a>
										</div>
										<div class="item">
										  <a href=""><img src="images/product-details/similar1.jpg" alt=""></a>
										  <a href=""><img src="images/product-details/similar2.jpg" alt=""></a>
										  <a href=""><img src="images/product-details/similar3.jpg" alt=""></a>
										</div>

									</div>

								  <!-- Controls -->
								  <a class="left item-control" href="#similar-product" data-slide="prev">
									<i class="fa fa-angle-left"></i>
								  </a>
								  <a class="right item-control" href="#similar-product" data-slide="next">
									<i class="fa fa-angle-right"></i>
								  </a>
							</div>
						</div>
						
						<form method= "GET"action="/G8104wallapop/loginPage" id="noLogueado"></form>
						<form method= "POST"action="${url}" id="logueado"></form>
						<div class="col-sm-7">
							<div class="product-information"><!--/product-information-->
								<h2>${product.getTitle()}</h2>
								<span>
									<span>${product.getPrice()}&euro;</span>
									
									<%if(session.getAttribute("user") == null){%>
										<button type= "submit"  class="btn btn-fefault cart" form="noLogueado">Contactar con el vendedor</button>
									<%}else{%>
										<button type= "submit"  class="btn btn-fefault cart" form="logueado">Contactar con el vendedor</button>
									<%}%>	
								</span>
								<p><b>Estado del producto:</b> ${product.getState()}</p>
								<p><b>Categoria:</b> ${product.getCategory()}</p>
								<p><b>Vendedor: </b>${product.getUsuario().getName()} ${product.getUsuario().getFirstsurname()}</p>
								<p><b>Ubicaci&oacute;n: </b> ${product.getUsuario().getCity()}</p>
								
							</div><!--/product-information-->
						</div>
					</div><!--/product-details-->
					
					<div class="category-tab shop-details-tab"><!--category-tab-->
						<div class="col-sm-12">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#reviews" data-toggle="tab">Descripci&oacute;n del Producto</a></li>
							</ul>
						</div>
						<div class="tab-content">
							
							<div class="tab-pane fade active in" id="tag" >
								<div class="col-sm-12">
									<p>${product.getDescription()}</p>
								</div>
							</div>
						</div>
					</div><!--/category-tab-->

				</div>
			</div>
		</div>
	</section>

	<footer id="footer"><!--Footer-->


    <div class="footer-widget">
				<div class="container">
					<div class="row">
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Servicios</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Ayuda</a></li>
									<li><a href="">Contacto</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Comprar</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Telefon&iacute;a</a></li>
									<li><a href="">Ropa</a></li>
									<li><a href="">Tecnolog&iacute;a</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Pol&iacute;tica</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Terminos de uso</a></li>
									<li><a href="">Pol&iacute;tica de privacidad</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="single-widget">
								<h2>Sobre nosotros</h2>
								<ul class="nav nav-pills nav-stacked">
									<li><a href="">Informaci&oacute;n de la compa&ntilde;&iacute;a</a></li>
									<li><a href="">Localizaci&oacute;n</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-3 col-sm-offset-1">
						</div>

					</div>
				</div>
			</div>

		<div class="footer-bottom">
			<div class="container">
				<div class="row">
				<p class="pull-left">Copyright © 2013 WALLAPOP Inc. All rights reserved.</p>
				</div>
			</div>
		</div>

	</footer><!--/Footer-->



    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
    <script src="js/displayWindow.js"></script>
</body>
	</body>
</html>